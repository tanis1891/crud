const mongoose = require('mongoose')

const MONGODB_URL = process.env.MONGODB_URL;

mongoose.connect(MONGODB_URL, {
    useUnifiedTopology: true,
    useNewUrlParser: true
})
    .then(db => console.log('La base de datos está conectada'))
    .catch(err => console.log(err));