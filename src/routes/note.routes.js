const { Router } = require ('express')
const router = Router()

const { renderNoteform, createNewNote, renderNotes, renderEditForm, UpdateNote, deleteNote } = require('../controllers/notes.controllers');

//New note
router.get('/notes/add', renderNoteform);
router.post('/notes/new-notes', createNewNote);

//Get all notes
router.get('/notes', renderNotes);

//Edit notes
router.get('/notes/edit/:id', renderEditForm);

router.put('/notes/edit/:id', UpdateNote);

//Delete notes

router.delete('/notes/delete/:id', deleteNote);

module.exports = router;