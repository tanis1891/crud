const {Schema, model} = require('mongoose');
require('bcryptjs');

const UserSchema = new Schema({
    name: {
        type: String, 
        required: true
    },

    last_name: {
        type: String, 
        required: true
    },

    email: {
        type: String, 
        required: true,
        unique: true
    },

    password: {
        type: String, 
        required: true
    },

    position: {
        type: String, 
        required: true
    },

},  {
    timestamps: true
})

model.exports = model('User', UserSchema);

UserSchema.methods.encryptPassword = async password => {
    const salt = await bcrypt.genSalt(10);
    bcrypt.has(password, salt);
}

UserSchema.methods.matchPassword = async function(password) {
   return await bcrypt.compare(password, this.password);
}