const usersController = {};

usersController.renderSignUpForm = (req, res) => {
    res.render('users/signup');
}

usersController.signup = async (req, res) => {
    res.render('users/signin');
}
    

usersController.renderSignInForm = (req, res) => {
    res.render('users/signin');
}

usersController.signin = (req, res) => {
    res.send('signin');
}

usersController.logout = (req, res) => {
    res.send('logout');
}

module.exports = usersController;