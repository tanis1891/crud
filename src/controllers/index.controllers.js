const indexControl = {};

indexControl.renderIndex = (req, res) => {
    res.render('index')
};

indexControl.renderAbout = (req, res) => {
    res.render('about')
};

module.exports = indexControl;