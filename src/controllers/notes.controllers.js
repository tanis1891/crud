const notesControl = {};

const Note = require('../models/Note');

notesControl.renderNoteform = (req, res) => {
    res.render('./notes/new-notes');
}

notesControl.createNewNote = async (req, res) => {
    const{title, description} = req.body;
    const newNote = new Note({title, description});
    await newNote.save();
    req.flash('success_msg', 'Note added successfully');
    res.redirect('/notes')

}

notesControl.renderNotes = async (req, res) => {
    const notes = await Note.find();
    res.render('./notes/all-notes', {notes})
}

notesControl.renderEditForm = async (req, res) => {
    const note = await Note.findById(req.params.id);
    res.render('./notes/edit-notes', {note});
}

notesControl.UpdateNote = async (req, res) => {
    const{ title, description} = req.body;
    await Note.findByIdAndUpdate(req.params.id, {title, description});
    req.flash('success_msg', 'Note updated successfully');
    res.redirect('/notes')
}

notesControl.deleteNote = async (req, res) => {
    await Note.findByIdAndDelete(req.params.id);
    req.flash('success_msg', 'Note deleted successfully');
    res.redirect('/notes')
}
module.exports = notesControl;